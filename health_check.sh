env=$1
url=$2
healthy=false
limit=20
counter=0
wait=1

echo "Checking ingress backends health..."

while ! $healthy && [[ $counter -lt $limit ]]; do
    status=$(kubectl get ing --namespace=$env -o=jsonpath='{.items[*].metadata.annotations.ingress\.kubernetes\.io/backends}')

    if [[ $status == *"HEALTHY"* ]]; then
        echo "Backends healthy!"
        healthy=true
    else
        echo "Backends not healthy yet, waiting $(($wait))min ..."
        ((counter++))
        sleep $(($wait))m
    fi

done

if [[ $counter -ge $limit ]]; then
    echo "Timeout! Ingress backends not healthy after $(($wait * $limit)) min..."
    exit 62
else
    echo "Ingress backends healthy after $(($wait * $counter)) min..."

    echo "Checking ingress certificate..."

    healthy=false
    counter=0

    while ! $healthy && [[ $counter -lt $limit ]]; do
        issuer=$(cut -d'=' -f4 <<< "$(curl --insecure -vvI $url 2>&1 | grep 'issuer:')")

        echo "Issuer is: <$issuer>"

        if [[ $issuer == *"cert-manager.local"* ]] ||  [ -z "$issuer" ]; then
            echo "Not ready yet, waiting $(($wait))min ..."
            ((counter++))
            sleep $(($wait))m
        else
            echo "Certificate ready!"
            healthy=true
        fi
    done

    if [[ $counter -ge $limit ]]; then
        echo "Timeout! Certificate Ingress not ready after $(($wait * $limit)) min..."
        exit 62
    else
        exit 0
    fi
fi