variable "gcp_credentials_dev" {
  type = string
  description = "Location of service account for GCP Development"
}

variable "gcp_project_id_dev" {
  type = string
  description = "GCP Project id in Development"
}

variable "gcp_region_dev" {
  type = string
  description = "GCP Region in Development"
}

variable "gke_cluster_name_dev" {
  type = string
  description = "GCP Cluster name in Development"
}

variable "gke_zone_dev" {
  type = string
  description = "Zone for GKE cluster in Development"
}

variable "gke_node_pool_name_dev" {
  type = string
  description = "GKE node pool name in Development"
}

variable "gke_node_pool_count_dev" {
  type = number
  description = "GKE node pool count in Development"
}

variable "gke_service_account_name_dev" {
  type = string
  description = "GKE service account name in Development"
}

variable "gitlab_access_token" {
  type = string
  description = "GitLab Token"
}

variable "currency_converter_api_name_dev" {
  type = string
  description = "Currency Converter api name in Development"
}

variable "currency_converter_api_image_dev" {
  type = string
  description = "Currency Converter api image name in Development"
}

variable "currency_converter_ui_name_dev" {
  type = string
  description = "Currency Converter UI Name in Development"
}

variable "currency_converter_ui_image_dev" {
  type = string
  description = "Currency Converter UI image name in Development"
}

variable "ghost_app_name_dev" {
  type = string
  description = "Ghost Name in Development"
}

variable "ghost_password_dev" {
  type = string
  description = "Ghost password in Development"
}

variable "ghost_db_root_password_dev" {
  type = string
  description = "Ghost db root password in Development"
}

variable "ghost_db_password_dev" {
  type = string
  description = "Ghost db password in Development"
}

variable "cluster_issuer_name_dev" {
  type = string
  description = "Cluster issuer name in Development"
}

variable "cluster_issuer_email_dev" {
  type = string
  description = "Cluster issuer email in Development"
}

variable "cluster_issuer_server_dev" {
  type = string
  description = "Cluster issuer server in Development"
}

variable "cluster_issuer_private_key_secret_name_dev" {
  type = string
  description = "Cluster issuer private key secret name in Development"
}

variable "ingress_name_dev" {
  type = string
  description = "Ingress name in Development"
}

variable "ingress_static_ip_name_dev" {
  type = string
  description = "Ingress static IP name in Development"
}

variable "ghost_hostname_dev" {
  type = string
  description = "Ghost hostname in Development"
}

variable "currency_converter_hostname_dev" {
  type = string
  description = "Currency Converter hostname in Development"
}

variable "home_hostname_dev" {
  type = string
  description = "Home hostname in Development"
}

variable "home_name_dev" {
  type = string
  description = "Home app name in Development"
}

variable "home_image_dev" {
  type = string
  description = "Home image in Development"
}