module "cluster" {
    source = "../modules/cluster"
    gcp_credentials = var.gcp_credentials_dev
    gcp_project_id = var.gcp_project_id_dev
    gcp_region = var.gcp_region_dev
    gke_cluster_name = var.gke_cluster_name_dev
    gke_zone = var.gke_zone_dev
    gke_node_pool_name = var.gke_node_pool_name_dev
    gke_node_pool_count = var.gke_node_pool_count_dev
    gke_service_account_name = var.gke_service_account_name_dev
}

module "home" {
    depends_on = [module.cluster]
    source = "../modules/app"
    deployment_name = var.home_name_dev
    deployment_image = var.home_image_dev
    service_name = "${module.home.service_name}"
}

module "currency_converter_api" {
    depends_on = [module.cluster]
    source = "../modules/app"
    deployment_name = var.currency_converter_api_name_dev
    deployment_image = var.currency_converter_api_image_dev
    service_name = "${module.currency_converter_api.service_name}"
}

module "currency_converter_ui" {
    depends_on = [module.cluster,module.currency_converter_api]
    source = "../modules/app"
    deployment_name = var.currency_converter_ui_name_dev
    service_type = "NodePort"
    deployment_image = var.currency_converter_ui_image_dev
    service_name = "${module.currency_converter_ui.service_name}"
}

module "ghost" {
    depends_on = [module.cluster]
    source = "../modules/ghost"
    ghost_host = var.ghost_hostname_dev
    ghost_app_name = var.ghost_app_name_dev
    ghost_password = var.ghost_password_dev
    ghost_db_root_password = var.ghost_db_root_password_dev
    ghost_db_password = var.ghost_db_password_dev
    ghost_service_name = "${module.ghost.ghost_service_name}"
}

module "ssl" {
    depends_on = [module.cluster]
    source = "../modules/ssl"
    cluster_issuer_name = var.cluster_issuer_name_dev
    cluster_issuer_email = var.cluster_issuer_email_dev
    cluster_issuer_server = var.cluster_issuer_server_dev
    cluster_issuer_private_key_secret_name = var.cluster_issuer_private_key_secret_name_dev
}

module "ingress" {
    depends_on = [module.cluster,module.home,module.currency_converter_api,module.currency_converter_ui,module.ghost,module.ssl]
    source = "../modules/ingress"
    ingress_name = var.ingress_name_dev
    ingress_static_ip_name = var.ingress_static_ip_name_dev
    ingress_cluster_issuer_name = "${module.ssl.cluster_issuer_name}"
    cluster_issuer_private_key_secret_name = "${module.ssl.cluster_issuer_private_key_secret_name}"
    ingress_rules = [
      {
        hostname    = var.ghost_hostname_dev
        service_name = "${module.ghost.ghost_service_name}"
        service_port = "${module.ghost.ghost_service_port}"
      },
      {
        hostname    = var.currency_converter_hostname_dev
        service_name = "${module.currency_converter_ui.service_name}"
        service_port = "${module.currency_converter_ui.service_port}"
      },
      {
        hostname    = var.home_hostname_dev
        service_name = "${module.home.service_name}"
        service_port = "${module.home.service_port}"
      }
    ]
}