terraform {
    backend "http" {
    }
    required_version = ">= 0.13"
    required_providers {
      gitlab = {
          source = "gitlabhq/gitlab"
          version = "3.6.0"
      }
      kubectl = {
          source  = "gavinbunney/kubectl"
          version = ">= 1.7.0"
      }
      helm = {
          source = "hashicorp/helm"
          version = "2.2.0"
      }
    }
}

data "google_client_config" "default" {}

provider "google" {
    credentials = base64decode(var.gcp_credentials_dev)
    project = var.gcp_project_id_dev
    region = var.gcp_region_dev
}

provider "kubernetes" {
  host                   = "https://${module.cluster.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.cluster.certificate)
}

/*
provider "gitlab" {
    token = var.gitlab_access_token
}
*/

provider "kubectl" {
  host                   = "https://${module.cluster.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.cluster.certificate)
  load_config_file       = false
}

provider "helm" {
  kubernetes {
    host                   = "https://${module.cluster.endpoint}"
    token                  = data.google_client_config.default.access_token
    cluster_ca_certificate = base64decode(module.cluster.certificate)
  }
}