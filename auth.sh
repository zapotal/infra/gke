echo $gcp_credentials >> encode.txt 
base64 -d encode.txt >> credentials.json
gcloud auth activate-service-account $gke_service_account_name --key-file=credentials.json --project=$gcp_project_id
gcloud container clusters get-credentials $gke_cluster_name --zone $gke_zone --project $gcp_project_id