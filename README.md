[[_TOC_]]

# Introduction
This is a personal project designed to showcase my DevOps skills to the world. 

### What?
This a IaC project in GKE developed using technologies like `terraform`, `gcloud`, `docker`, `gitlab`, `kubernetes`, `kubectl`, `cert-manager`, `helm`, `bash`, `.Net5`, `reactjs` and more.
It hosts 4 different containerized applications that run in dev and prod environments under **zapotal.club** domain.

### Why?
A project like this allows you to showcase samples of work you have done which serves as a digital resume and proof you have the skills that you say you have in your resume.

### Who?
Hi there, my name is Brandon Alvarez and I've worked in software engineering since 2015. My commitment to critical thinking and attention to detail have gotten me to where I am today: a Senior DevOps Engineer. I have a passion for infrastructure, security and automation, and I'm an experienced team leader who typically manages 5 developers at any given time.

Prior to my senior role, I worked as a .Net software engineer, a InfoSec developer and a Cloud developer. Because I've already held all this other positions, I have keen insight into what it takes to run a successful project. My career goal is to become a CTO, and I know this role would help me hone my leadership skills.

### Where?
I'm from Costa Rica, a beautiful tropical country in Central America. I love my country and the talent that resides here, this is one of the reasons why I mostly look forward for remote positions. After COVID19 remote work has become part of our reality and I personally think is the way to go for now on.

> You may be wonder what *zapotal* means. That is the name of my childhood home town, I just find it lovely to use that name for this project.


 # Infraestructure

 ### Diagram

 ![alt text](archi.png "GCP Architecture")

 ### Description
 The project is hosted in Google Cloud Platform where we have a GKE cluster in region **us-central1**, zone **us-central1-c**.
 Inside the cluster we have two namespaces: **default** and **cert-manager**. 

 Whithin default namespace you'll find and **Ingress**. This is the point of entry from the outside world to the apps in the cluster. This Ingress redirects network traffic to the corresponding service (**blog**, **money**, **home**) and then to the apps. Each service takes the traffic of a different DNS entry.  

* **blog service**:

   Redirects traffic from *blog.zapotal.club* to the blog deployment.

* **money service**:

   Redirects traffic from *money.zapotal.club* to the money deployment.

* **home service**

   Redirects traffic from *zapotal.club* to the home deployment.

There is a deployment for each of the apps available in the project.


* **blog deployment**:

  This is a blog based on [Ghost](https://ghost.org/), which is a free and open source blogging platform written in JavaScript and distributed under the MIT License, designed to simplify the process of online publishing for individual bloggers as well as online publications.

  The deployment contains the ghost app and a mysql instance to persist data. The comunication between ghost and mysql happens internally using the blog service.

* **money deployment**:

   A free currency converter to calculate exchange rates for currencies. Built using react webhooks.

   The deployment contains and api and a react ui. The comunication between ui and api happens internally using the money service.

* **home deployment**
   
   Zapotal home page where you will find all available zapotal websites. It's a simple .Net5 + ReactJs project.



Whithin **cert-manager** namespace you'll find the **webhook** and cert-manager **services** with their corresponding pods. Inside this namespace you'll also find the **ca-injector** pod.

cert-manager adds certificates and certificate issuers as resource types in Kubernetes clusters, and simplifies the process of obtaining, renewing and using those certificates.
It will issue certificates from Let’s Encrypt and will ensure certificates are valid and up to date, and attempt to renew certificates at a configured time before expiry.


 # Terraform

 The project uses terraform to provide and manage the infrastructure. It also uses terraform modules to create lightweight abstractions, so that you can describe your infrastructure in terms of its architecture, rather than directly in terms of physical objects.  

 ### Modules


 * **[cluster](/modules/cluster)**:

   Creates the GKE cluster and its node pools. You can configure fields like region, zone, cluster name and node pool count.

* **[app](/modules/app)**:

   Creates a deployment with an exposing service in the GKE cluster. You can configure fields like deployment name, version, image, replicas, namespace, service name, service port and service type.

* **[ghost](/modules/ghost)**

   Creates a ghost deployment using helm provider. You can configure fields like ghost app name, namespace, ghost password, ghost host, ghost service name, ghost service port and ghost service type.

* **[ingress](/modules/ingress)**:

   Creates a ingress in the GKE cluster that redirects traffict to specific services. You can configure fields like ingress rules, ingress name, static IP, cluster issuer and private key.

* **[ssl](/modules/ssl)**
   Creates a cluster issuer and configures cert-manager to handle tls in the cluster.  You can configure fields like cluster issuer server and the certificates private key.


 ### Providers

 The project uses 4 providers:

 * **google**:

   The Google provider is used to configure your Google Cloud Platform infrastructure.

* **kubernetes**:

   Management of all Kubernetes resources, including Deployments, Services, Custom Resources (ClusterIssuer and Orders), Ingress, Secrets and more.

* **kubectl**

   This provider is the best way of managing Kubernetes resources in Terraform, by allowing you to use the thing Kubernetes loves best - yaml!

* **helm**:

   Manage installed Charts in your Kubernetes cluster through Terraform in the same way Helm does.

* **gitlab**:

   The GitLab provider is used to interact with GitLab group or user resources.

 ### Variables

 Variables serve as parameters for a Terraform module, allowing aspects of the module to be customized without altering the module's own source code, and allowing modules to be shared between different configurations. 
 
 All variable values in this project are specified as GitLab CICD variables following the pattern TF_VAR_[var_name] so that the gitlab provider can use them.

 ### Environments
 The project has two environments: [dev](/dev) and [prod](/prod). The state for each environment is managed by [GitLab](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html).

 The GitLab managed Terraform state backend can store your Terraform state easily and securely, and spares you from setting up additional remote resources like Amazon S3 or Google Cloud Storage. Its features include:

* Versioning of Terraform state files.
* Supporting encryption of the state file both in transit and at rest.
* Locking and unlocking state.
* Remote Terraform plan and apply execution.




