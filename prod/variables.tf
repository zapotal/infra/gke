variable "gcp_credentials_prod" {
  type = string
  description = "Location of service account for GCP Production"
}

variable "gcp_project_id_prod" {
  type = string
  description = "GCP Project id in Production"
}

variable "gcp_region_prod" {
  type = string
  description = "GCP Region in Production"
}

variable "gke_cluster_name_prod" {
  type = string
  description = "GCP Cluster name in Production"
}

variable "gke_zone_prod" {
  type = string
  description = "Zone for GKE cluster in Production"
}

variable "gke_node_pool_name_prod" {
  type = string
  description = "GKE node pool name in Production"
}

variable "gke_node_pool_count_prod" {
  type = number
  description = "GKE node pool count in Production"
}

variable "gke_service_account_name_prod" {
  type = string
  description = "GKE service account name in Production"
}

variable "gitlab_access_token" {
  type = string
  description = "GitLab Token"
}

variable "currency_converter_api_name_prod" {
  type = string
  description = "Currency Converter api name in Production"
}

variable "currency_converter_api_image_prod" {
  type = string
  description = "Currency Converter api image name in Production"
}

variable "currency_converter_ui_name_prod" {
  type = string
  description = "Currency Converter UI Name in Production"
}

variable "currency_converter_ui_image_prod" {
  type = string
  description = "Currency Converter UI image name in Production"
}

variable "ghost_app_name_prod" {
  type = string
  description = "Ghost Name in Production"
}

variable "ghost_password_prod" {
  type = string
  description = "Ghost password in Production"
}

variable "ghost_db_root_password_prod" {
  type = string
  description = "Ghost db root password in Production"
}

variable "ghost_db_password_prod" {
  type = string
  description = "Ghost db password in Production"
}

variable "cluster_issuer_name_prod" {
  type = string
  description = "Cluster issuer name in Production"
}

variable "cluster_issuer_email_prod" {
  type = string
  description = "Cluster issuer email in Production"
}

variable "cluster_issuer_server_prod" {
  type = string
  description = "Cluster issuer server in Production"
}

variable "cluster_issuer_private_key_secret_name_prod" {
  type = string
  description = "Cluster issuer private key secret name in Production"
}

variable "ingress_name_prod" {
  type = string
  description = "Ingress name in Production"
}

variable "ingress_static_ip_name_prod" {
  type = string
  description = "Ingress static IP name in Production"
}

variable "ghost_hostname_prod" {
  type = string
  description = "Ghost hostname in Production"
}

variable "currency_converter_hostname_prod" {
  type = string
  description = "Currency Converter hostname in Production"
}

variable "home_hostname_prod" {
  type = string
  description = "Home hostname in Production"
}

variable "home_name_prod" {
  type = string
  description = "Home app name in Production"
}

variable "home_image_prod" {
  type = string
  description = "Home image in Production"
}