resource "kubectl_manifest" "ingress" {
    yaml_body = <<YAML
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ${var.ingress_name}
  annotations:
    kubernetes.io/ingress.global-static-ip-name: ${var.ingress_static_ip_name}
    cert-manager.io/cluster-issuer: ${var.ingress_cluster_issuer_name}
    acme.cert-manager.io/http01-edit-in-place: "true"
spec:
  tls: 
  - hosts:
    %{ for r in var.ingress_rules }
    - ${r.hostname}
    %{ endfor }
    secretName: ${var.cluster_issuer_private_key_secret_name}
  rules:
  %{ for r in var.ingress_rules }
  - host: ${r.hostname}
    http:
      paths:
      - path: "/*"
        pathType: ImplementationSpecific
        backend:
          service:
            name: ${r.service_name}
            port:
              number: ${r.service_port}
  %{ endfor }
YAML
}