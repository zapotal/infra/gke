variable "ingress_rules" {
  type = list
  description = "List of ingress details like hostname, service and port"
}

variable "ingress_name" {
  default = "zapotal-ingress"
  type = string
  description = "Ingress Name"
}

variable "ingress_static_ip_name" {
  type = string
  description = "Ingress Static IP Name"
}

variable "ingress_cluster_issuer_name" {
  type = string
  description = "Ingress Cluster Issuer Name"
}

variable "cluster_issuer_private_key_secret_name" {
  type = string
  description = "Name of a secret used to store the ACME account private key"
}

