/* kubectl Apps */

resource "kubernetes_namespace" "namespace" {
  count = var.namespace != "default" ? 1 : 0
  metadata {
    name = var.namespace
  }
}

resource "kubernetes_deployment" "app" {
  depends_on = [kubernetes_namespace.namespace]
  metadata {
    name = var.deployment_name
    labels = {
      app = var.deployment_name
      version = var.deployment_version
    }
  }

  spec {
    replicas = var.deployment_replicas
    selector {
      match_labels = {
        app = var.deployment_name
        version = var.deployment_version
      }
    }

    template {
      metadata {
        labels = {
          app = var.deployment_name
          version = var.deployment_version
        }
      }

      spec {
        container {
          name              = var.deployment_name
          image             = var.deployment_image
          image_pull_policy = "IfNotPresent"
          port {
            container_port = var.deployment_port
          }
        }

        restart_policy                   = "Always"
        termination_grace_period_seconds = 30
      }
    }

    strategy {
      type = "RollingUpdate"

      rolling_update {
        max_surge = "1"
      }
    }

    min_ready_seconds = 45
  }
}


resource "kubernetes_service" "service" {
  metadata {
    name = "${var.deployment_name}-service"
  }

  spec {
    type = var.service_type
    port {
      name        = "http"
      protocol    = "TCP"
      port        = var.service_port
      target_port = var.deployment_port
    }

    selector = {
      app = var.deployment_name
      version = var.deployment_version
    }
  }
}