output "service_name" {
  value = "${kubernetes_service.service.metadata[0].name}"
}

output "service_port" {
  value = "${var.service_port}"
}