variable "deployment_name" {
  type = string
  description = "Deployment Name"
}

variable "deployment_port" {
  default = 80
  type = number
  description = "Deployment Port"
}

variable "deployment_version" {
  default = "v1"
  type = string
  description = "Deployment Version"
}

variable "deployment_image" {
  type = string
  description = "Deployment Image"
}

variable "deployment_replicas" {
  default = 1
  type = number
  description = "Deployment Replicas"
}

variable "namespace" {
  default = "default"
  type = string
  description = "Deployment and Service Namespace"
}

variable "service_name" {
  type = string
  description = "Service Name"
}

variable "service_port" {
  default = 80
  type = number
  description = "Service Port"
}

variable "service_type" {
  default = "ClusterIP"
  type = string
  description = "Service Type"
}
