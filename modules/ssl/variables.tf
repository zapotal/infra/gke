
variable "cluster_issuer_name" {
  type = string
  description = "Cluster Issuer Name, used for annotations"
}

variable "cluster_issuer_email" {
  type = string
  description = "Email address used for ACME registration"
}

variable "cluster_issuer_server" {
  type = string
  description = "The ACME server URL"
}

variable "cluster_issuer_private_key_secret_name" {
  type = string
  description = "Name of a secret used to store the ACME account private key"
}

