resource "kubernetes_namespace" "cert_manager" {
  metadata {
    name = "cert-manager"
  }
}

resource "helm_release" "cert_manager" {
  depends_on = [kubernetes_namespace.cert_manager]
  name       = "cert-manager"
  namespace  = kubernetes_namespace.cert_manager.metadata[0].name
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  set {
    name  = "installCRDs" 
    value = "true"
  }
}

resource "kubectl_manifest" "cluster_issuer" {
    yaml_body = <<YAML
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: ${var.cluster_issuer_name}
spec:
  acme:
    server: ${var.cluster_issuer_server}
    email: ${var.cluster_issuer_email}
    privateKeySecretRef:
      name: ${var.cluster_issuer_private_key_secret_name}
    solvers:
    - http01:
        ingress:
            class: ingress-gce
YAML
    depends_on = [kubernetes_namespace.cert_manager,helm_release.cert_manager]
}
