output "cluster_issuer_name" {
  value = "${var.cluster_issuer_name}"
}

output "cluster_issuer_private_key_secret_name" {
  value = "${var.cluster_issuer_private_key_secret_name}"
}