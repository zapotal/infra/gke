output "endpoint" {
  value = module.gke.endpoint
}

output "certificate" {
  value = module.gke.ca_certificate
}
