module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google"
  project_id                 = var.gcp_project_id
  name                       = var.gke_cluster_name
  region                     = var.gcp_region
  regional                   = false
  zones                      = [var.gke_zone]
  network                    = "default"
  subnetwork                 = "default"
  ip_range_pods              = ""
  ip_range_services          = ""
  http_load_balancing        = true
  horizontal_pod_autoscaling = true
  network_policy             = false
  remove_default_node_pool   = true

  node_pools = [
    {
      name                      = var.gke_node_pool_name
      machine_type              = "e2-medium"
      min_count                 = 1
      max_count                 = var.gke_node_pool_count
      local_ssd_count           = 0
      disk_size_gb              = 100
      disk_type                 = "pd-standard"
      image_type                = "cos_containerd"
      auto_repair               = true
      auto_upgrade              = true
      service_account           = var.gke_service_account_name
      preemptible               = false
      initial_node_count        = var.gke_node_pool_count
    },
  ]
}
