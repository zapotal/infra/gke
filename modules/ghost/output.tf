output "ghost_service_name" {
  value = "${var.ghost_app_name}"
}

output "ghost_service_port" {
  value = "${var.ghost_service_port}"
}