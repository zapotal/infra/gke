variable "ghost_app_name" {
  default = "ghost"
  type = string
  description = "Ghost App Name"
}

variable "helm_repository" {
  default = "https://charts.bitnami.com/bitnami"
  type = string
  description = "Helm Repository"
}

variable "helm_chart" {
  default = "ghost"
  type = string
  description = "Helm Chart"
}

variable "ghost_password" {
  type = string
  description = "Ghost Admin Password"
}

variable "ghost_blog_title" {
  default = "Zapotal Blog"
  type = string
  description = "Ghost Blog title"
}

variable "ghost_host" {
  type = string
  description = "Ghost host"
}

variable "ghost_db_root_password" {
  type = string
  description = "MariaDB root password"
}

variable "ghost_db_password" {
  type = string
  description = "Custom user password"
}

variable "ghost_service_name" {
  type = string
  description = "Ghost service name"
}

variable "ghost_service_port" {
  default = 80
  type = string
  description = "Ghost service port"
}

variable "ghost_service_type" {
  default = "NodePort"
  type = string
  description = "Ghost service type"
}

variable "namespace" {
  default = "default"
  type = string
  description = "Deployment and Service Namespace"
}
