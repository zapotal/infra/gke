resource "helm_release" "ghost_app" {
  name       = var.ghost_app_name
  repository = var.helm_repository
  chart      = var.helm_chart
  namespace  = var.namespace

  set {
    name  = "installCRDs" 
    value = "true"
  }

  set {
    name  = "ghostUsername"
    value = "admin"
  }

  set {
    name  = "ghostPassword"
    value = var.ghost_password
  }

  set {
    name  = "ghostBlogTitle"
    value = var.ghost_blog_title
  }

  set {
    name  = "ghostHost"
    value = var.ghost_host
  }

  set {
    name  = "service.type"
    value = var.ghost_service_type
  }

  set {
    name  = "service.port"
    value = var.ghost_service_port
  }

  set {
    name  = "mariadb.auth.rootPassword"
    value = var.ghost_db_root_password
  }

  set {
    name  = "mariadb.auth.password"
    value = var.ghost_db_password
  }

}